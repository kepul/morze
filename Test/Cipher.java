import dataBase.DataBase;
import org.junit.Assert;
import org.junit.Test;
import sample.Constants;
import sample.ControllerJournal;
import java.sql.SQLException;


public class Cipher {
    @Test(expected = ExceptionInInitializerError.class)
    public void checkShowJournal() throws SQLException {
        ControllerJournal controllerJournal = new ControllerJournal();

        controllerJournal.createWindow();
    }

    @Test()
    public void checkAlphabet() {
        Constants.setMorseVocabularyEncrypt();

        Assert.assertFalse(".-", Boolean.parseBoolean(Constants.morseMap.get("a")));
    }

    @Test(expected = Exception.class)
    public void checkInsert() throws SQLException {
        DataBase dataBase = new DataBase();

        dataBase.insert(String.valueOf(123), String.valueOf(123));
    }
}