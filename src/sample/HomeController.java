package sample;

import dataBase.DataBase;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;

import java.sql.SQLException;

// класс отвечаюший за взоимодействие с полями ввода и выводв
// и входом в журнал
public class HomeController {

    @FXML
    private TextField inputFild;

    @FXML
    private TextField outputFilde;


    public void cipher(ActionEvent actionEvent) throws SQLException {
        outputFilde.setText("");
        Constants.setMorseVocabularyEncrypt();
        String[] letters = inputFild.getText().split("");
        StringBuilder output = new StringBuilder();
        for(String letter : letters){
            output.append(Constants.morseMap.get(letter));
        }
        outputFilde.setText(String.valueOf(output));
        DataBase dataBase = new DataBase();
        dataBase.insert(inputFild.getText(), String.valueOf(output));
    }

    public void jornal(ActionEvent actionEvent) {


        ControllerJournal controllerJournal = new ControllerJournal();
        try {
            controllerJournal.createWindow();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    }

