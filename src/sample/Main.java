package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

//главный класс создает стартовое окно
public class Main extends Application {
    Parent start;

    @Override
    public void start(Stage primaryStage) throws Exception{
        start = FXMLLoader.load(getClass().getResource("app.fxml"));
        primaryStage.setTitle("Morze");
        primaryStage.setScene(new Scene(start, 800, 600));
        primaryStage.show();
    }


    public static void main(String[] args) {

        launch(args);
    }
}
