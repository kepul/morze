package dataBase;

public interface MySQLDataBaseConfig {

    String URL = "jdbc:mysql://localhost:3306/baza_morze?serverTimezone=Europe/Moscow&useSSL=false";
    String USERNAME = "root";
    String PASSWORD = "";
    String DRIVER_NAME = "com.mysql.cj.jdbc.Driver";
}
