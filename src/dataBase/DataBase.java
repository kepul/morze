package dataBase;


import java.sql.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Date;

public class DataBase implements MySQLDataBaseConfig {

    private static Connection connection;

    static {
        try {
            connection = getConnection();
        } catch (Exception ignored) {
        }
    }

    private static Connection getConnection() throws Exception {
        Connection connection;

        Class.forName(DRIVER_NAME).getDeclaredConstructor().newInstance();
        connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);

        return connection;
    }

    public void insert(String input, String output) throws SQLException {
        Date date = new Date();

        String currentDate = date.getYear() + "-" + date.getMonth() + "-" + date.getDate();

        String query = "INSERT INTO cipher (Date, input, output) VALUES (" +
                "\"" + currentDate + "\", " +
                "\"" + input+ "\", " +
                "\"" + output + "\");";

        Statement statement = connection.createStatement();
        statement.executeUpdate(query);
    }

    public List<String> select() throws SQLException {
        List<String> result = new ArrayList<>();
        String query = "SELECT * FROM cipher;";
        ResultSet resultSet = connection.createStatement().executeQuery(query);

        while (resultSet.next()) {
            String data = "Date: " + resultSet.getString("Date") + " / " +
                    "input: " + resultSet.getString("input") + " / " +
                    "output " + resultSet.getString("output") + ";";
            result.add(data);
        }

        return result;
    }

    public void closeConnection() throws SQLException {
        connection.close();
    }
}
